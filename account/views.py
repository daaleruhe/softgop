from django.shortcuts import render

from django.conf import settings
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.contrib.auth import views as auth_views
from django.contrib.auth.mixins import LoginRequiredMixin

from django.urls import reverse_lazy

from .forms import RegistroForm


# if not request.user.is_authenticated:
#         return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

class LoginView(auth_views.LoginView):
    redirect_authenticated_user = True


class LogoutView(auth_views.LogoutView):
    pass


class AccountList(LoginRequiredMixin, ListView):
    model = User
    template_name = "accounts/index.html"


class AccountCreate(LoginRequiredMixin, CreateView):
    template_name = "accounts/create.html"
    success_url = reverse_lazy("account:list")
    form_class = RegistroForm
    model = User

class AccountUpdate(LoginRequiredMixin, UpdateView):
    model = User
    form_class = RegistroForm
    success_url = reverse_lazy('account:list')
    template_name = 'accounts/update.html'

class AccountDelete(LoginRequiredMixin, DeleteView):
    model = User
    success_url = reverse_lazy('account:list')
    template_name = 'accounts/delete.html'
