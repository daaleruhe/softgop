import re
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError


class RegistroForm(UserCreationForm):
    error_css_class = 'is-invalid'

    fields_only_letters = [
    ]

    fields_required = [
        'first_name', 'last_name',
        'email'

    ]

    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            #'is_staff',
        ]

        labels = {
            'username':'Nombre de usuario',
        }

        help_texts = {
            'username':'',
            'first_name': 'Ingresar solo letras',
            'last_name': 'Ingresar solo letras',
            'email': 'Ingresar un formato correcto',
        }

    def __init__(self, *args, **kwargs):
        super(RegistroForm, self).__init__(*args, **kwargs)

        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True
        
        for visible in self.visible_fields():
            attrs = {'class': 'form-control', 'placeholder': visible.label}

            if visible.name in self.fields_only_letters:
                attrs.update({'pattern': "[-a-zA-Z_]+"})

            visible.field.widget.attrs.update(attrs)

            if self.errors.get(visible.name, None):
                visible.field.widget.attrs.update({'class': 'form-control is-invalid'})
