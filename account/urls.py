from django.urls import path

from .views import AccountCreate, AccountList, LoginView, LogoutView,AccountUpdate,AccountDelete


app_name = 'account'

urlpatterns = [
    path('', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('list/', AccountList.as_view(), name='list'),
    path('create/', AccountCreate.as_view(), name='create'),
    path('update/<int:pk>/', AccountUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', AccountDelete.as_view(), name='delete'),
]
