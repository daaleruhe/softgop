from django.urls import path

from .views import WorkValidList, WorkFinishedList, WorkExpiredList, WorkCreate, WorkDelete, WorkUpdate, WorkFinish
from .reports.work import ReportWorkView

app_name = 'work'
urlpatterns = [
    path('', WorkValidList.as_view(), name='list-vigentes'),
    path('expired/', WorkExpiredList.as_view(), name='list-exprired'),
    path('finished/', WorkFinishedList.as_view(), name='list-finished'),
    path('create/', WorkCreate.as_view(), name='create'),
    path('delete/<int:pk>/', WorkDelete.as_view(), name='delete'),
    path('update/<int:pk>/', WorkUpdate.as_view(), name='update'),
    path('finish/<int:pk>/', WorkFinish.as_view(), name='finish'),
    path('report/<int:pk>/', ReportWorkView.as_view(), name='report'),
]
