import re
from django.forms import ModelForm
from django.core.exceptions import ValidationError

from .models import Work

class WorkForm(ModelForm):
    error_css_class = 'is-invalid'

    fields_only_letters = [
        'tipo'
    ]

    class Meta:
        model = Work
        fields = '__all__'
        exclude = ('estado', 'multa')

    
    def __init__(self, *args, **kwargs):
        super(WorkForm, self).__init__(*args, **kwargs)

        for visible in self.visible_fields():
            attrs = {'class': 'form-control', 'placeholder': visible.label}
            if visible.name in self.fields_only_letters:
                attrs.update({'pattern': "[-a-zA-Z_]+"})
            
            if visible.name == 'fecha_de_inicio':
                attrs.update()

            visible.field.widget.attrs.update(attrs)

            if self.errors.get(visible.name, None):
                visible.field.widget.attrs.update({'class': 'form-control is-invalid'})


class WorkFinishForm(ModelForm):
    class Meta:
        model = Work
        fields = ('estado',)
    