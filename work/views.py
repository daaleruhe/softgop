from django.views.generic import ListView, CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

from django.urls import reverse_lazy

from .models import Work
from .forms import WorkForm, WorkFinishForm
from .utils import calcular_multa


class WorkValidList(LoginRequiredMixin, ListView):
    model = Work
    template_name = "Works/index.html"
    queryset = Work.estados.valids()


class WorkExpiredList(LoginRequiredMixin, ListView):
    model = Work
    template_name = "Works/index.html"
    queryset = Work.estados.expireds()

    def get_queryset(self):
        queryset = super(WorkExpiredList, self).get_queryset()

        for query in queryset:
        
            multa = calcular_multa(query)
            query.valor_multa(multa)

        return queryset


class WorkFinishedList(LoginRequiredMixin, ListView):
    model = Work
    template_name = "Works/index.html"
    queryset = Work.estados.finisheds()


class WorkCreate(LoginRequiredMixin, CreateView):
    model = Work
    form_class = WorkForm
    success_url = reverse_lazy('work:list-vigentes')
    template_name = 'works/form.html'


class WorkDelete(LoginRequiredMixin, DeleteView):
    model = Work
    success_url = reverse_lazy('work:list-vigentes')
    template_name = 'works/delete.html'


class WorkUpdate(LoginRequiredMixin, UpdateView):
    model = Work
    form_class = WorkForm
    success_url = reverse_lazy('work:list-vigentes')
    template_name = 'works/update.html'


class WorkFinish(LoginRequiredMixin, UpdateView):
    model = Work
    form_class = WorkFinishForm
    success_url = reverse_lazy('work:list-finished')
    template_name = 'works/update.html'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.finalizar()
        form = self.get_form()
        return self.form_valid(form)
