from django.db import models
from contractor.models import Contractor
from penalty.models import Penalty

from .managers import WorkManager


class Work(models.Model):
    nombre= models.CharField(
        max_length=60, 
        unique=True,
        help_text="Ingresar solo letras")

    direccion = models.CharField(
        max_length=30)
        
    tipo = models.CharField(
        max_length=15,
        help_text="Ingresar solo letras")

    valor = models.IntegerField(
        help_text="Ingresar solo números")
    
    multa = models.IntegerField(default=0)

    fecha_de_inicio = models.DateField(
        help_text="Ingresar la fecha dd MM yy")

    fecha_de_fin = models.DateField(
        help_text="Ingresar la fecha dd MM yy")

    estado = models.BooleanField(
        "Activo", 
        default=True)

    contratista = models.ForeignKey(
        Contractor, 
        on_delete=models.CASCADE,
        help_text="Ingrese nombre del contratista encargado")

    reglas = models.ForeignKey(
        Penalty, 
        on_delete=models.CASCADE,
        help_text="Ingrese los porcentajes de multa")


    objects = models.Manager()
    estados = WorkManager()


    def mostrar_estado(self):
        if self.estado:
            mostrar_estado = "Activo"
        else:
            mostrar_estado = "Inactivo"
        
        return mostrar_estado

    def __str__(self):
        cadena = "{0}, {1}"
        return cadena.format(
            self.nombre, 
            self.direccion)

    def finalizar(self):
        if self.estado:
            self.estado = False
            self.save(update_fields=['estado'])

    def valor_multa(self, multa):
        if multa > 0:
            self.multa = multa
            self.save(update_fields=['multa'])
