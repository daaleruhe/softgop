import io

from datetime import date

from django.conf import settings
from django.views.generic import DetailView
from django.http import FileResponse

from babel.numbers import format_currency

from reportlab.lib import colors
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.platypus import Table, TableStyle, Paragraph
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch, cm, mm

from ..models import Work


class ReportWorkView(DetailView):
    model = Work
    content_type = 'application/pdf'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        buffer = self.get_context_data(object=self.object)

        return FileResponse(buffer, as_attachment=False, filename='hello.pdf')

    def get_context_data(self, **kwargs):
        object = self.object

        data = [['Nombre', object.nombre],
                ['Dirección', object.direccion],
                ['Tipo', object.tipo],
                ['Valor', format_currency(object.valor, 'COP', locale='es_CO')],
                ['Multa', format_currency(object.multa, 'COP', locale='es_CO')],
                ['Fecha de inicio', '{:%d, %B %Y}'.format(object.fecha_de_inicio)],
                ['Fecha de fin', '{:%d, %B %Y}'.format(object.fecha_de_fin)],
                ['Estado', object.mostrar_estado()],
                ['Contratista', object.contratista.nombre_completo()]]
        data_len = len(data)

        buffer = io.BytesIO()

        styles = getSampleStyleSheet()
        style = styles["BodyText"]

        p = canvas.Canvas(buffer,
                          pagesize=A4)

        p.setLineWidth(.3)
        p.setFont('Helvetica', 22)
        p.drawString(30, 750, 'SoftGop Informes')

        p.setFont('Helvetica', 12)
        p.drawString(30, 730, '{} ({})'.format(object.nombre, object.id))

        p.setFont('Helvetica', 12)
        p.drawString(480, 750, '{:%d, %B %Y}'.format(date.today()))
        p.line(460, 747, 580, 747)

        header = Paragraph("<bold><font size=18>Información</font></bold>", style)

        t = Table(data, colWidths=[5 * cm, 10 * cm])
        t.setStyle(TableStyle([("BOX", (0, 0), (-1, -1), 0.25, colors.black),
                               ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                               ('ALIGN', (0, 0), (-1, -1), "CENTER")]))

        for each in range(data_len):
            if each % 2 == 0:
                bg_color = colors.whitesmoke
            else:
                bg_color = colors.white

            t.setStyle(TableStyle([('BACKGROUND', (0, each), (-1, each), bg_color)]))

        aW = 600
        aH = 650

        w, h = header.wrap(aW, aH)
        header.drawOn(p, 72, aH)
        aH = aH - h
        w, h = t.wrap(aW, aH)
        t.drawOn(p, 72, aH - h)

        p.showPage()
        p.save()

        buffer.seek(0)
        return buffer
