from datetime import date


__all__ = ('days_between', 'calcular_multa')

hoy = date.today()


def days_between(d1, d2):
    return abs(d2 - d1).days


def calcular_multa(contrato):
    dias_contrato = days_between(contrato.fecha_de_inicio, contrato.fecha_de_fin)
    dias_atrazados = days_between(contrato.fecha_de_fin, hoy)
    reglas = contrato.reglas
    porcentaje_desfase = reglas.porcentaje_desfase
    porcentaje_valor_desfase = reglas.porcentaje_valor_desfase
    porcentaje_valor_multa = reglas.porcentaje_valor_multa

    try:
        porcentaje_atrazado = (dias_atrazados * 100) / dias_contrato
    except ZeroDivisionError:
        porcentaje_atrazado = 0

    valor_multa = (contrato.valor * porcentaje_valor_multa) / 100
    valor_defase = 0
    while porcentaje_atrazado > porcentaje_desfase:
        valor_defase += (contrato.valor * porcentaje_valor_desfase) / 100
        porcentaje_desfase += 10

    valor_total = valor_multa + valor_defase

    return round(valor_total, 2)
