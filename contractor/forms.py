import re
from django.forms import ModelForm
from django.core.exceptions import ValidationError

from .models import Contractor



class ContractorForm(ModelForm):
    error_css_class = 'is-invalid'

    fields_only_letters = [
        'primer_nombre', 'segundo_nombre', 
        'primer_apellido', 'segundo_apellido'
    ]

    class Meta:
        model = Contractor
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ContractorForm, self).__init__(*args, **kwargs)

        for visible in self.visible_fields():
            attrs = {'class': 'form-control', 'placeholder': visible.label}
            if visible.name in self.fields_only_letters:
                attrs.update({'pattern': "[-a-zA-Z_]+"})

            visible.field.widget.attrs.update(attrs)

            if self.errors.get(visible.name, None):
                visible.field.widget.attrs.update({'class': 'form-control is-invalid'})
