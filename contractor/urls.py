from django.urls import path

from .views import ContractorList, ContractorCreate, ContractorDelete,ContractorUpdate

app_name = 'contractor'
urlpatterns = [
    path('', ContractorList.as_view(), name='list'),
    path('create/', ContractorCreate.as_view(), name='create'),
    path('delete/<int:pk>/', ContractorDelete.as_view(), name='delete'),
    path('update/<int:pk>/', ContractorUpdate.as_view(), name='update'),
]
