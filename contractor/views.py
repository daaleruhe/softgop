from django.views.generic import ListView, CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import Contractor
from .forms import ContractorForm


class ContractorList(LoginRequiredMixin, ListView):
    model = Contractor
    template_name = "contractors/index.html"


class ContractorCreate(LoginRequiredMixin, CreateView):
    model = Contractor
    form_class = ContractorForm
    success_url = reverse_lazy('contractor:list')
    template_name = 'contractors/form.html'


class ContractorDelete(LoginRequiredMixin, DeleteView):
    model = Contractor
    success_url = reverse_lazy('contractor:list')
    template_name = 'contractors/delete.html'


class ContractorUpdate(LoginRequiredMixin, UpdateView):
    model = Contractor
    form_class = ContractorForm
    success_url = reverse_lazy('contractor:list')
    template_name = 'contractors/update.html'
