from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.ui import WebDriverWait
#from selenium import webdriver
#from selenium.webdriver.common.keys import keys

from .models import Contractor

# Para ejecutar un solo test es: python manage.py test contractor.tests.ContractorTestCase
# Para ejecutar un metodo dentro del test: python manage.py test contractor.tests.ContractorViewTest

class ContractorTestCase(TestCase):
    def setUp(self):
        Contractor.objects.create(primer_nombre="fabian", segundo_nombre = "", primer_apellido="hernandez", segundo_apellido="fernandez", documento_identidad=123, direccion="calle123", telefono=123 )
        Contractor.objects.create(primer_nombre="antonio", segundo_nombre = "jesus", primer_apellido="lopez", segundo_apellido="lopera", documento_identidad=455, direccion="calle123", telefono=777 )

    def test_nombre_completo(self):
        sin_segundo_nombre = Contractor.objects.get(documento_identidad=123)
        self.assertEqual(sin_segundo_nombre.nombre_completo(), 'hernandez fernandez fabian')

        con_segundo_nombre = Contractor.objects.get(documento_identidad=455)
        self.assertEqual(con_segundo_nombre.nombre_completo(), 'lopez lopera antonio jesus')

class ContractorViewTest(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(100)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_tittle(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/contractors/'))
        self.assertIn('Contractors', self.selenium.title)

    def test_create(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/contractors/'))
        self.selenium.find_element_by_id('IdCreate').click()

        Primer_Nombre_input = self.selenium.find_element_by_name("primer_nombre")
        Primer_Nombre_input.send_keys('David')

        Segundo_Nombre_input = self.selenium.find_element_by_name("segundo_nombre")
        Segundo_Nombre_input.send_keys('Alejandro')

        Primer_Apellido_input = self.selenium.find_element_by_name("primer_apellido")
        Primer_Apellido_input.send_keys('Rua')

        Segundo_Apellido_input = self.selenium.find_element_by_name("segundo_apellido")
        Segundo_Apellido_input.send_keys('Herrera')

        Documento_input = self.selenium.find_element_by_name("documento_identidad")
        Documento_input.send_keys('322540150')

        Direccion_input = self.selenium.find_element_by_name("direccion")
        Direccion_input.send_keys('calle 108 # 68a - 76')

        Telefono_input = self.selenium.find_element_by_name("telefono")
        Telefono_input.send_keys('322190234')

        self.selenium.find_element_by_id('IdAgregar').click()

        #mensaje_error = "documento de identidad es erroneo"

        self.assertIn('Contractors', self.selenium.title)

        print ("Se agrego el contratista.")

        

        

        

    
