from django.contrib import admin

from .models import Penalty

# Register your models here.

@admin.register(Penalty)
class PenaltyAdmin(admin.ModelAdmin):
    pass 