from django.urls import path

from .views import PenaltyList, PenaltyCreate, PenaltyDelete, PenaltyUpdate

app_name = 'penalty'
urlpatterns = [
    path('', PenaltyList.as_view(), name='list'),
    path('create/', PenaltyCreate.as_view(), name='create'),
    path('delete/<int:pk>/', PenaltyDelete.as_view(), name='delete'),
    path('update/<int:pk>/', PenaltyUpdate.as_view(), name='update')
]
