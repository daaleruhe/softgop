from django.views.generic import ListView, CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import Penalty
from .forms import PenaltyForm
# Create your views here.

class PenaltyList(LoginRequiredMixin, ListView):
    model = Penalty
    template_name = "penalty/index.html"

class PenaltyCreate(LoginRequiredMixin, CreateView):
    model = Penalty
    form_class = PenaltyForm
    success_url = reverse_lazy('penalty:list')
    template_name = 'penalty/form.html'

class PenaltyDelete(LoginRequiredMixin, DeleteView):
    model = Penalty
    success_url = reverse_lazy('penalty:list')
    template_name = 'penalty/delete.html'

class PenaltyUpdate(LoginRequiredMixin, UpdateView):
    model = Penalty
    form_class = PenaltyForm
    success_url = reverse_lazy('penalty:list')
    template_name = 'Penalty/update.html'
